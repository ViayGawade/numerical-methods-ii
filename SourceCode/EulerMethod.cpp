#include<math.h>
#include<iostream>
#include <fstream>
#include "../HeaderFiles/Euler.hpp"
#include "../HeaderFiles/CommanFuntions.hpp"
using namespace std;

void euler_method(int eg_no)
{
	double Y0=1,X0=0;
	double Ynext,Ycurr,Xcurr;

	if(eg_no==1)
	{
		cout<<"By Analytical, Y(1)="<<(exp(2))<<".\n";
		for(int i=1;i<=500;i++)
		{
			double h=i*0.0002;
			Xcurr=X0;
			Ycurr=Y0;
			while(Xcurr<1.0-h)
			{
				Ynext=Ycurr+h*(2*Ycurr);
				//cout<<"Xcurr= "<<Xcurr<<"\t Ycurr= "<<Ycurr<<"\n";
				Xcurr+=h;
				Ycurr=Ynext;
			}
			double error=(exp(2))-Ynext;
			cout<<"Y(1)="<<Ynext<<"\t with error "<<fabs(error)<<"   when h="<<h<<" .\n";
			wrf_Step_vs_Error("Outputs/EulerMethod/Eg1_Step_vs_Error.txt",h,fabs(error));
			wrf_Step_vs_Y("Outputs/EulerMethod/Eg1_Step_vs_Y.txt",h,Ynext);
		}
	}
	else if(eg_no==2)
	{
		cout<<"By Analytical, Y(1)="<<(exp(-1))<<".\n";
		for(int i=1;i<=500;i++)
		{
			double h=i*0.0002;
			Xcurr=X0;
			Ycurr=Y0;
			while(Xcurr<1.0-h)
			{
				Ynext=Ycurr+h*(-2*Ycurr*Xcurr);
				//cout<<"Xcurr= "<<Xcurr<<"\t Ycurr= "<<Ycurr<<"\n";
				Xcurr+=h;
				Ycurr=Ynext;
			}
			double error=(exp(-1))-Ynext;
			cout<<"Y(1)="<<Ynext<<"\t with error "<<fabs(error)<<"   when h="<<h<<" .\n";
			wrf_Step_vs_Error("Outputs/EulerMethod/Eg2_Step_vs_Error.txt",h,fabs(error));
			wrf_Step_vs_Y("Outputs/EulerMethod/Eg2_Step_vs_Y.txt",h,Ynext);
		}
	}
	else if (eg_no==3)
	{
		cout<<"By Analytical, Y(1)=0.5 .\n";
		for(int i=1;i<=500;i++)
		{
			double h=i*0.0002;
			Xcurr=X0;
			Ycurr=Y0;
			while(Xcurr<1.0-h)
			{
				Ynext=Ycurr+h*(-2*Ycurr*Ycurr*Xcurr);
				//cout<<"Xcurr= "<<Xcurr<<"\t Ycurr= "<<Ycurr<<"\n";
				Xcurr+=h;
				Ycurr=Ynext;
			}
			double error=0.5-Ynext;
			cout<<"Y(1)="<<Ynext<<"\t with error "<<fabs(error)<<"   when h="<<h<<" .\n";
			wrf_Step_vs_Error("Outputs/EulerMethod/Eg3_Step_vs_Error.txt",h,fabs(error));
			wrf_Step_vs_Y("Outputs/EulerMethod/Eg3_Step_vs_Y.txt",h,Ynext);
		}
	}
}