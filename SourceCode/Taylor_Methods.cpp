#include<iostream>
#include <math.h>
#include "../HeaderFiles/Taylor.hpp"
#include "../HeaderFiles/CommanFuntions.hpp"
using namespace std;

void taylor_method(int eg_no)
{
	taylor_second_order(eg_no);	
	taylor_third_order(eg_no);
}

void taylor_second_order(int eg_no)
{
	/*
			f(xn,yn)=y'

			2nd order:
				Yn+1=Y(xn)+h*f(xn,yn)+(h^2/2)*f'(xn,yn)
	*/

	double Y0=1,X0=0;
	double Ynext,Ycurr,Xcurr;
	if(eg_no==1)
	{
		for(int i=1;i<=50;i++)
		{
			double h=i*0.002;
			Xcurr=X0;
			Ycurr=Y0;
			while(Xcurr<1.0-h)
			{
				Ynext=Ycurr+h*(2*Ycurr)+(h*h/2.0)*(4*Ycurr);
				//cout<<"Xcurr= "<<Xcurr<<"\t Ycurr= "<<Ycurr<<"\n";
				Xcurr+=h;
				Ycurr=Ynext;
			}
			double error=(exp(2))-Ynext;
			cout<<"Y(1)="<<Ynext<<"\t with error "<<fabs(error)<<" .\n";
			wrf_Step_vs_Error("Outputs/TaylorSecondOrder/Eg1_Step_vs_Error.txt",h,fabs(error));
			wrf_Step_vs_Y("Outputs/TaylorSecondOrder/Eg1_Step_vs_Y.txt",h,Ynext);
		}

	}
	else if(eg_no==2)
	{
		for(int i=1;i<=50;i++)
		{
			double h=i*0.002;
			Xcurr=X0;
			Ycurr=Y0;
			while(Xcurr<1.0-h)
			{
				Ynext=Ycurr+h*(-2*Xcurr*Ycurr)+(h*h/2)*(exp(-Xcurr*Xcurr)*(4*Xcurr*Xcurr-2));
				//cout<<"Xcurr= "<<Xcurr<<"\t Ycurr= "<<Ycurr<<"\n";
				Xcurr+=h;
				Ycurr=Ynext;
			}
			double error=(exp(-1))-Ynext;
			cout<<"Y(1)="<<Ynext<<"\t with error "<<fabs(error)<<" .\n";
			wrf_Step_vs_Error("Outputs/TaylorSecondOrder/Eg2_Step_vs_Error.txt",h,fabs(error));
			wrf_Step_vs_Y("Outputs/TaylorSecondOrder/Eg2_Step_vs_Y.txt",h,Ynext);
		}
	}
	else if(eg_no==3)
	{

	}
}

void taylor_third_order(int eg_no)
{
	/*
			f(xn,yn)=y'

			3rd order:
				Yn+1=Y(xn)+h*f(xn,yn)+(h^2/2)*f'(xn,yn)+(h^3/6)*f''(xn,yn)
	*/

	double Y0=1,X0=0;
	double Ynext,Ycurr,Xcurr;
	if(eg_no==1)
	{
		cout<<"By Analytical, Y(1)="<<(exp(2))<<".\n";
		for(int i=1;i<=50;i++)
		{
			double h=i*0.002;
			Xcurr=X0;
			Ycurr=Y0;
			while(Xcurr<1.0-h)
			{
				Ynext=Ycurr+h*(2*Ycurr)+(h*h/2.0)*(4*Ycurr)+(pow(h,3)/6.0)*(8*exp(-pow(Xcurr,2)));
				//cout<<"Xcurr= "<<Xcurr<<"\t Ycurr= "<<Ycurr<<"\n";
				Xcurr+=h;
				Ycurr=Ynext;
			}
			double error=(exp(2))-Ynext;
			cout<<"Y(1)="<<Ynext<<"\t with error "<<fabs(error)<<" .\n";
			wrf_Step_vs_Error("Outputs/TaylorThirdOrder/Eg1_Step_vs_Error.txt",h,fabs(error));
			wrf_Step_vs_Y("Outputs/TaylorThirdOrder/Eg1_Step_vs_Y.txt",h,Ynext);
		}
	}
	else if(eg_no==2)
	{
		cout<<"By Analytical, Y(1)="<<(exp(-1))<<".\n";
		for(int i=1;i<=50;i++)
		{
			double h=i*0.002;
			Xcurr=X0;
			Ycurr=Y0;
			while(Xcurr<1.0-h)
			{
				Ynext=Ycurr+h*(-2*Xcurr*Ycurr)+(h*h/2.0)*(4*pow(Xcurr,2)-2)*exp(-Xcurr*Xcurr)+(pow(h,3)/6.0)*(-(8*pow(Xcurr,3)-12*Xcurr)*exp(-pow(Xcurr,2)));
				//cout<<"Xcurr= "<<Xcurr<<"\t Ycurr= "<<Ycurr<<"\n";
				Xcurr+=h;
				Ycurr=Ynext;
			}
			double error=(exp(-1))-Ynext;
			cout<<"Y(1)="<<Ynext<<"\t with error "<<fabs(error)<<" .\n";
			wrf_Step_vs_Error("Outputs/TaylorThirdOrder/Eg1_Step_vs_Error.txt",h,fabs(error));
			wrf_Step_vs_Y("Outputs/TaylorThirdOrder/Eg1_Step_vs_Y.txt",h,Ynext);
		}
	}
}