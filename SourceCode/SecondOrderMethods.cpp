#include<math.h>
#include<iostream>
#include <fstream>
#include "../HeaderFiles/SecondOrderMethods.hpp"
#include "../HeaderFiles/CommanFuntions.hpp"
using namespace std;

void improved_Tangent_Method(int eg_no)
{
	double Y0=1,X0=0;
	double Ynext,Ycurr,Xcurr;

	if(eg_no==1)
	{
		cout<<"By Analytical, Y(1)="<<(exp(2))<<".\n";
		for(int i=1;i<=500;i++)
		{
			double h=i*0.0002;
			Xcurr=X0;
			Ycurr=Y0;
			while(Xcurr<1.0-h)
			{
				double k1=h*(2*Ycurr);
				double k2=h*(2*(Ycurr+k1/2));
				Ynext=Ycurr+k2;
				cout<<"Xcurr= "<<Xcurr<<"\t Ycurr= "<<Ycurr<<"\n";
				Xcurr+=h;
				Ycurr=Ynext;
			}
			double error=(exp(2))-Ynext;
			cout<<"Y(1)="<<Ynext<<"\t with error "<<fabs(error)<<"   when h="<<h<<" .\n";
			wrf_Step_vs_Error("Outputs/ImprovedTangentMethod/Eg1_Step_vs_Error.txt",h,fabs(error));
			wrf_Step_vs_Y("Outputs/ImprovedTangentMethod/Eg1_Step_vs_Y.txt",h,Ynext);
		}
	}
	else if(eg_no==2)
	{
		cout<<"By Analytical, Y(1)="<<(exp(-1))<<".\n";
		for(int i=1;i<=500;i++)
		{
			double h=i*0.0002;
			Xcurr=X0;
			Ycurr=Y0;
			while(Xcurr<1.0-h)
			{
				double k1=h*(-2*Xcurr*Ycurr);
				double k2=h*(-2*(Xcurr+h/2)*(Ycurr+k1/2));
				Ynext=Ycurr+k2;
				cout<<"Xcurr= "<<Xcurr<<"\t Ycurr= "<<Ycurr<<"\n";
				Xcurr+=h;
				Ycurr=Ynext;
			}
			double error=(exp(-1))-Ynext;
			cout<<"Y(1)="<<Ynext<<"\t with error "<<fabs(error)<<"   when h="<<h<<" .\n";
			wrf_Step_vs_Error("Outputs/ImprovedTangentMethod/Eg2_Step_vs_Error.txt",h,fabs(error));
			wrf_Step_vs_Y("Outputs/ImprovedTangentMethod/Eg2_Step_vs_Y.txt",h,Ynext);
		}
	}
	else if(eg_no==3)
	{
		cout<<"By Analytical, Y(1)=0.5 \n";
		for(int i=1;i<=500;i++)
		{
			double h=i*0.0002;
			Xcurr=X0;
			Ycurr=Y0;
			while(Xcurr<1.0-h)
			{
				double k1=h*(-2*Xcurr*Ycurr*Ycurr);
				double k2=h*(-2*(Xcurr+h/2)*pow((Ycurr+k1/2),2));
				Ynext=Ycurr+k2;
				cout<<"Xcurr= "<<Xcurr<<"\t Ycurr= "<<Ycurr<<"\n";
				Xcurr+=h;
				Ycurr=Ynext;
			}
			double error=0.5-Ynext;
			cout<<"Y(1)="<<Ynext<<"\t with error "<<fabs(error)<<"   when h="<<h<<" .\n";
			wrf_Step_vs_Error("Outputs/ImprovedTangentMethod/Eg2_Step_vs_Error.txt",h,fabs(error));
			wrf_Step_vs_Y("Outputs/ImprovedTangentMethod/Eg2_Step_vs_Y.txt",h,Ynext);
		}
	}
}


void euler_cauchy_method(int eg_no)
{
	double Y0=1,X0=0;
	double Ynext,Ycurr,Xcurr;

	if(eg_no==1)
	{
		cout<<"By Analytical, Y(1)="<<(exp(2))<<".\n";
		for(int i=1;i<=500;i++)
		{
			double h=i*0.0002;
			Xcurr=X0;
			Ycurr=Y0;
			while(Xcurr<1.0-h)
			{
				double k1=h*(2*Ycurr);
				double k2=h*(2*(Ycurr+k1));
				Ynext=Ycurr+k2;
				cout<<"Xcurr= "<<Xcurr<<"\t Ycurr= "<<Ycurr<<"\n";
				Xcurr+=h;
				Ycurr=Ynext;
			}
			double error=(exp(2))-Ynext;
			cout<<"Y(1)="<<Ynext<<"\t with error "<<fabs(error)<<"   when h="<<h<<" .\n";
			wrf_Step_vs_Error("Outputs/EulerCauchyMethod/Eg1_Step_vs_Error.txt",h,fabs(error));
			wrf_Step_vs_Y("Outputs/EulerCauchyMethod/Eg1_Step_vs_Y.txt",h,Ynext);
		}
	}
	else if(eg_no==2)
	{
		cout<<"By Analytical, Y(1)="<<(exp(-1))<<".\n";
		for(int i=1;i<=500;i++)
		{
			double h=i*0.0002;
			Xcurr=X0;
			Ycurr=Y0;
			while(Xcurr<1.0-h)
			{
				double k1=h*(-2*Xcurr*Ycurr);
				double k2=h*(-2*(Xcurr+h)*(Ycurr+k1));
				Ynext=Ycurr+k2;
				cout<<"Xcurr= "<<Xcurr<<"\t Ycurr= "<<Ycurr<<"\n";
				Xcurr+=h;
				Ycurr=Ynext;
			}
			double error=(exp(-1))-Ynext;
			cout<<"Y(1)="<<Ynext<<"\t with error "<<fabs(error)<<"   when h="<<h<<" .\n";
			wrf_Step_vs_Error("Outputs/EulerCauchyMethod/Eg2_Step_vs_Error.txt",h,fabs(error));
			wrf_Step_vs_Y("Outputs/EulerCauchyMethod/Eg2_Step_vs_Y.txt",h,Ynext);
		}
	}
	else if(eg_no==3)
	{
		cout<<"By Analytical, Y(1)=0.5 \n";
		for(int i=1;i<=500;i++)
		{
			double h=i*0.0002;
			Xcurr=X0;
			Ycurr=Y0;
			while(Xcurr<1.0-h)
			{
				double k1=h*(-2*Xcurr*Ycurr*Ycurr);
				double k2=h*(-2*(Xcurr+h)*pow((Ycurr+k1),2));
				Ynext=Ycurr+k2;
				cout<<"Xcurr= "<<Xcurr<<"\t Ycurr= "<<Ycurr<<"\n";
				Xcurr+=h;
				Ycurr=Ynext;
			}
			double error=0.5-Ynext;
			cout<<"Y(1)="<<Ynext<<"\t with error "<<fabs(error)<<"   when h="<<h<<" .\n";
			wrf_Step_vs_Error("Outputs/EulerCauchyMethod/Eg2_Step_vs_Error.txt",h,fabs(error));
			wrf_Step_vs_Y("Outputs/EulerCauchyMethod/Eg2_Step_vs_Y.txt",h,Ynext);
		}
	}
}