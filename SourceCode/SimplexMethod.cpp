#include "../HeaderFiles/Simplex.hpp"
#include<iostream>
#include <iomanip> 
#include <limits>
using namespace std;
void Simplex::maximizeZ()
{
	int irreration=1;
	while(1)
	{
		/*displayTable(irreration);*/

		// calculate Zj
		for(int i = 0; i < column; i++)
		{
			// calculate Zj
			double a=0,b=0;
			for(int j=0;j<row;j++)
			{
				if(Z[C[j]]!=-1000)
					a+=Z[C[j]]*A[j][i];
				else
					b+=-A[j][i];
			}
			
			// calculate Zj-Cj
			if(Z[i]==-1000)
			{
				diff[i].a=a;
				diff[i].b=(1000*b-Z[i])/1000;
			}		
			else
			{
				diff[i].a=a-Z[i];
				diff[i].b=b;	
			}
		}
				
		// find key column
		// if all M's coeiffient > 0 then stop 
		int flag=0;
		for(int i=0;i<column;i++)
			if(diff[i].b<=0)
				flag=1;
		if(flag==0)
		{
			displayTable(irreration);
			cout<<"#All (Zj-Cj) are non-negative !\n";
			break;
		}

		int minM_pos=-1;
		double minM=0;
		// check if all  M's coeiffient are 0
		flag=0;
		for(int i=0;i<column;i++)
			if(diff[i].b!=0)
				flag=1;

		if (flag==0)
		{
			// check min(a) 
			for(int i=0;i<column;i++)
				if(diff[i].a<0 && diff[i].a<minM)
				{
					minM=diff[i].a;
					minM_pos=i;
				}
		}
		else
		{
			// check min(b).
			for(int i=0;i<column;i++)
				if(diff[i].b<0 && diff[i].b<minM)
				{
					minM=diff[i].b;
					minM_pos=i;
				}

		}
		
		// check non-negative (Zj-Cj) present or not
		if(minM_pos==-1)
		{
			displayTable(irreration);
			cout<<"#All (Zj-Cj) are non-negative !\n";
			break;
		}

		/*// check if all M's coeffient are positive
		int signM=0;
		for(int i = 0; i < column; i++)
			if(diff[i].b<=0)
				signM=-1;
		if(signM==0)
		{
			displayTable(irreration);
			cout<<"#All (Zj-Cj) are non-negative !\n";
			break;
		}	

		// check if all M's coeffient are '0'
		int zeroM=0;
		for(int i = 0; i < column; i++)
			if(diff[i].b!=0)
				signM=-1;
		if(zeroM!=0)
		{
			// find min M's coeeffient
			for(int i = 0; i < column; i++)
			{
				if (diff[i].b<0 && diff[i].b<minM)
				{
					flag=1;
					minM=diff[i].b;
					minM_pos=i;
				}
			}
		}*/


		/*for(int i = 0; i < column; i++)
		{
			if (diff[i].b<0 && diff[i].b<minM)
			{
				flag=1;
				minM=diff[i].b;
				minM_pos=i;
			}
			// if all M coeffient are '0' then (Zj-Cj) will be non-negative
			if(diff[i].b<0)
				posMflag=1;
		}*/

		/*if(posMflag==0)		
		{
			displayTable(irreration);
			cout<<"@All (Zj-Cj) are non-negative !\n";
			break;
		}*/

		// if all coeefient of M '0' then take min(a) 
		/*if(flag==0)*/
		/*else
		{
			cout<<"VJ1************\n";
			for(int i = 0; i < column; i++)
			{
				if (minM>diff[i].a)
				{
					minM=diff[i].a;
					minM_pos=i;
				}			
			}
		}*/

		// calculate Ratio
		for(int i = 0; i < row; i++)
		{
			if(A[i][minM_pos]!=0)
				Ratio[i]=B[i]/A[i][minM_pos];
			else
				Ratio[i]=std::numeric_limits<double>::infinity();
		}

		// find key row
		int minRatio_pos,minRatio;
		for(int i = 0; i < row; i++)
			if (Ratio[i]!=std::numeric_limits<double>::infinity() && BVtype[C[i]]!=1)
			{
				minRatio_pos=i;
				minRatio=Ratio[i];
				break;
			}

		// check same ratio ins not present if yes then check is it artificial
		// if aritifial then select it
		for(int i = 0; i < row; i++)
			if (Ratio[i]!=std::numeric_limits<double>::infinity() && BVtype[C[i]]!=1 && Ratio[i]==minRatio && i!=minRatio_pos && BVtype[C[i]]==3)
			{
				minRatio_pos=i;
				minRatio=Ratio[i];
				break;
			}

		//cout<<"Start Ratio:"<<minRatio<<"at "<<minRatio_pos+1<<"'th position.\n";
		for(int i = minRatio_pos+1; i < row; i++)
		{
			if (Ratio[i]!=std::numeric_limits<double>::infinity())
			{
				if (minRatio>Ratio[i] && BVtype[C[i]]!=1)
				{
					minRatio_pos=i;
					minRatio=Ratio[i];
				}
			}
		}

		displayTable(irreration);
		cout<<"Key column is "<< std::noshowpos<<minM_pos+1<<"'th column.\n";
		cout<<"Key Row is "<< std::noshowpos<<minRatio_pos+1<<"'th row.\n";
		//cout<<"min(Zj-Cj) is "<<diff[minM_pos].a<<std::showpos<<diff[minM_pos].b<<"*M.\n";
		//cout<<"min(row) is "<<minRatio_pos+1<<"\n"; 

		
		int keyEleRow=minRatio_pos,keyEleCol=minM_pos;
		// alter Xb for key Row
		B[keyEleRow]/=A[keyEleRow][keyEleCol];
		// alter coeiffient matrix for key Row.
		double keyEle=A[keyEleRow][keyEleCol];
		for(int j=0;j<column;j++)
			A[keyEleRow][j]/=keyEle;

		// store key Column in new array
		double keyColm[row];
		for(int j=0;j<row;j++)
			keyColm[j]=A[j][keyEleCol];


		for(int i = 0; i < row; i++)
		{
			// alter Xb;
			if(i!=keyEleRow)
			{
				//cout<<"For i="<<i<<"\t"<<B[i]<<"="<<B[i]<<"-"<<B[keyEleRow]<<"*"<< A[i][keyEleCol]<<"\n";
				B[i]=B[i]-A[i][keyEleCol]*B[keyEleRow];
			}
			// alter coeiffient matrix
			if(i!=keyEleRow)
			{
				for(int j=0;j<column;j++)
				{
					//cout<<"For A["<<i<<"]["<<j<<"]\t"<<A[i][j]<<"="<<A[i][j]<<"-"<<A[keyEleRow][j]<<"*"<<keyColm[i]<<"\n";
					A[i][j]=A[i][j]-A[keyEleRow][j]*keyColm[i];
					//cout<<"Working 3\n";
				}	
			}
		}

		// change CB
		C[keyEleRow]=keyEleCol;
		irreration++;

		// break loop if all (Zj-Cj)>=0
		flag=0;
		for(int i=0;i<column;i++)
			if(diff[i].b<0 || diff[i].a<0)
				flag=1;
		if(flag==0)
		{
			cout<<"All (Zj-Cj) are non-negative !\n";
			break;
		}	
		if(irreration>5)
		{
			cout<<"Max Iteration reached !\n";
			break;
		}
	}

	// calculate Variable values


}