#include "../HeaderFiles/CommanFuntions.hpp"
#include <fstream>
using namespace std;

void wrf_Step_vs_Error(char *fname,double step_size,double error)
{
	std::ofstream outfile;
  	outfile.open(fname,std::ios::out | std::ios_base::app);
  	outfile << step_size<<"  "<<error<<"\n";
}

void wrf_Step_vs_Y(char *fname,double step_size,double Y)
{
	std::ofstream outfile;
  	outfile.open(fname,std::ios_base::app);
  	outfile << step_size<<"  "<<Y<<"\n";	
}