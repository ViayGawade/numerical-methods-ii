#include "../HeaderFiles/Simplex.hpp"
#include<string.h>
#include <fstream>
#include<ios>
#include <iomanip> 
using namespace std;
Simplex::Simplex(void)
{

}

Simplex::~Simplex()
{

}

void Simplex::readProblem(std::string fname)
{
	std::ifstream inFile;
	inFile.open("Examples/SimplexMethod/"+fname);
	if (inFile.is_open())
	{
		inFile >> row >> column;
		A = new double*[row];
		for (int i = 0; i<row; i++)
			A[i] = new double[column];
		for (int i = 0; i < row; i++)
			for (int j = 0; j < column; j++)
				inFile >> A[i][j];

		Ratio=new double[row];
		for (int i = 0; i < row; i++)
			Ratio[i]=0;

		diff=new Zj_Cj[column];
		for (int i = 0; i<column; i++)
			diff[i].a=diff[i].b=0;

		Z=new double[column];
		for (int i = 0; i<column; i++)
			inFile >> Z[i];
			
		B=new double[row];
		for (int i = 0; i<row; i++)
			inFile >> B[i];

		C=new int[row];
		for (int i = 0; i<row; i++)
			inFile >> C[i];

		BVtype=new int[column];
		for (int i = 0; i<column; i++)
			inFile >> BVtype[i];

		std::cout << "Matrix values set Successfully !\n";
	}
	else
	{
		std::cout << "Cannot open input file.\n";
		exit(1);
	}	
	inFile.close();
}

void Simplex::printBVType(int pos)
{
	int BV=BVtype[pos];
	int no_of_occorance=0;
	for (int i = 0; i <=pos; ++i)
		if(BVtype[i]==BV)
			no_of_occorance++;
	cout<< std::noshowpos;
	if(BVtype[pos]==1)
		cout<<"X"<<no_of_occorance;
	else if(BVtype[pos]==2)
		cout<<"S"<<no_of_occorance;
	else if(BVtype[pos]==3)
		cout<<"A"<<no_of_occorance;
	else
		cout<<"error "<<pos<<"#  "<<BVtype[pos];
}

void Simplex::displayTable(int k)
{
	cout<<"\n\nIteration "<<std::noshowpos<<k<<endl;
	cout<<"-----------------";
	for(int i=0;i<column;i++)
		cout<<"--------";
	cout<<"----------------\n";

	cout<<"|       |   Cj  |";
	for(int i=0;i<column;i++)
		if(Z[i]==-1000)
			cout<<" "<<std::setfill(' ') << std::setw(5) <<"-M"<<" |";
		else
			cout<<" "<<std::setfill(' ') << std::setw(5) <<std::showpos<<Z[i]<<" |";
	cout<<"       |"<<"       |\n";

	cout<<"|  CB   ";	
	for(int i=0;i<column+1;i++)
		cout<<"|-------";
	cout<<"|  Xb   | Ratio ";
	cout<<"|\n";

	int a=1,b=1,c=1; 
	cout<<"|       |  B.V  |";
	for(int i=0;i<column;i++)
	{
		if(BVtype[i]==1)
			cout<<" "<<std::setfill(' ') << std::setw(4)<<"X"<< std::noshowpos<<a++<<" |";
		else if(BVtype[i]==2)
			cout<<" "<<std::setfill(' ') << std::setw(4)<<"S"<< std::noshowpos<<b++<<" |";
		else
			cout<<" "<<std::setfill(' ') << std::setw(4)<<"A"<< std::noshowpos<<c++<<" |";
	}
	cout<<"       |       |"<<endl;
	cout<<"-----------------";
	for(int i=0;i<column;i++)
		cout<<"--------";
	cout<<"----------------\n";
	
	// print value
	a=b=c=0;
	for (int i = 0; i < row; i++)
	{
		// print cost of variable
		if(Z[C[i]]==-1000)
			cout<<"| "<<std::setfill(' ') << std::setw(5)<< "-M"<<" |";
		else
			cout<<"| "<<std::setfill(' ') << std::setw(5)<< std::showpos<<Z[C[i]]<<" |";

		// print variable name
		cout<<" "<<std::setfill(' ') << std::setw(3);
		printBVType(C[i]);
		cout<<"  |";
		std::cout << std::setprecision(2);
		for (int j = 0; j < column; j++)
			cout<<" "<<std::setfill(' ') << std::setw(5) <<std::noshowpos<<A[i][j]<<" |";
		cout << resetiosflags( ios::fixed | ios::showpoint );	
		cout<<std::setfill(' ') << std::setw(7) <<std::noshowpos<< std::setprecision(4)<<B[i]<<"|";
		cout<<std::setfill(' ') << std::setw(7) <<std::noshowpos<<Ratio[i]<<"|"<<endl;
	}	
	cout<<"-----------------";
	for(int i=0;i<column;i++)
		cout<<"--------";
	cout<<"----------------\n";
	
	if(k>0)
	{
		// print Zj-Cj
		cout<<"|     Zj-Cj     |";
		// Zj=CB[r]*A[r][c] where c from 0 to column-1

		// store sum as  a+b*M
		
		for(int i = 0; i < column; i++)
		{
			std::cout << std::setprecision(3);
			cout<<std::setfill(' ') << std::setw(2)<<std::noshowpos<<diff[i].a<<std::showpos<<diff[i].b<<"*M |";
			cout << resetiosflags( ios::fixed | ios::showpoint );
			/*// calculate Zj
			int a=0,b=0;
			for(int j=0;j<row;j++)
			{
				if(Z[C[j]]!=-1000)
					a+=Z[C[j]]*A[j][i];
				else
					b+=-A[j][i];
			}
			
			// calculate Zj-Cj
			if(Z[i]==-1000)
				cout<<std::setfill(' ') << std::setw(2)<<std::noshowpos<<a<<std::showpos<<1000*b-Z[i]<<"*M |";
			else
				cout<<std::setfill(' ') << std::setw(2)<<std::noshowpos<<a-Z[i]<<std::showpos<<b<<"*M |";	*/
		}

		cout<<"       |       |";
		cout<<endl;
		cout<<"-----------------";
		for(int i=0;i<column;i++)
			cout<<"--------";
		cout<<"----------------\n";
	
	}
}