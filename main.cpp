#include <iostream>
#include <stdlib.h>
#include "HeaderFiles/Simplex.hpp"
#include "HeaderFiles/Euler.hpp"
#include "HeaderFiles/Taylor.hpp"
#include "HeaderFiles/SecondOrderMethods.hpp"
using namespace std;

int select_Problem()
{
	int eg_no=0;
	cout<<"Select Problem:\n";
	cout<<"1. Y'=2y  Y(0)=1. \n";
	cout<<"2. Y'=-2xy  Y(0)=1. \n";
	cout<<"3. Y'=2xy^2  Y(0)=1. \n";
	cout<<"4.Go Back\n";
	cout<<"\nEnter ur choice:";
	std::cin.clear();
	cin>>eg_no;

	return eg_no;
}

int main(int argc, char const *argv[])
{
	/* code */
	while(1)
	{
		system("clear");
		cout<<"\n\n****************** Welcome Linear Programming Problems ******************\n";
		cout<<"1. Numerical Optimization.\n";
		cout<<"2. Numerical Solutions of ODE.\n";
		cout<<"3. Numerical Solutions of PDE.\n";
		cout<<"4. Quit.\n";
		cout<<"\nEnter ur choice:";
		int choice;
		std::cin.clear();
		cin>>choice;
		system("clear");
		switch(choice)
		{
			case 1:
				{
					system("clear");
					cout<<"\n ***************** Numerical Optimization *****************\n";
					cout<<"1. Simplex Method.\n";
					cout<<"2. Go Back\n";
					cout<<"\nEnter ur choice:";
					int ch;
					std::cin.clear();
					cin>>ch;

					switch(ch)
					{
						case 1:
							{
								system("clear");
								cout<<"\n ***************** Simplex Method *****************\n";
								Simplex S;
								char myFile[20];
								std::cin.clear();
								cout<<"Enter File Name to read Problem: ";
								cin>>myFile;
								
								S.readProblem(myFile);
								S.displayTable(0);
								S.maximizeZ();
							}
							break;
						case 2:
							break;
						default:cout<<"Invalid choice !\n\n";
					}
					std::cout << "\nPress Any Key to Continue...";
					std::cin.clear();
					std::cin.get();
					std::cin.get();
				}
				break;
			case 2:
				{
					int flag=1;
					while(flag==1)
					{
						system("clear");
						cout<<"\n ***************** Numerical Solutions of ODE *****************\n";
						cout<<"1. Single Step Methods.\n";
						cout<<"2. Multistep Step Methods.\n";
						cout<<"3. Go Back\n";
						cout<<"\nEnter ur choice:";
						int ch;
						std::cin.clear();
						cin>>ch;

						switch(ch)
						{
							case 1:
								{
									int flag=1;
									while(flag==1)
									{
										system("clear");
										cout<<"\n ***************** Single Step Methods *****************\n";
										cout<<"1. Euler's Method.\n";
										cout<<"2. Taylar Series Method.\n";
										cout<<"3. For Second Order Methods.\n";
										cout<<"4. For Third Order Methods.\n";
										cout<<"5. For Forth Order Methods.\n";
										cout<<"6. Runge-Kutta Methods.\n";
										cout<<"7. Go Back\n";
										cout<<"\nEnter ur choice:";
										int ch;
										std::cin.clear();
										cin>>ch;

										switch(ch)
										{
											case 1:
												{
													system("clear");
													cout<<"\n ***************** Euler Method *****************\n";
													int ch =select_Problem();
													switch(ch)
													{
														case 1:
															euler_method(1);
															break;
														case 2:
															euler_method(2);
															break;
														case 3:
															euler_method(3);
															break;
														case 4:
															break;
														default:cout<<"Invalid choice !\n\n";
													}
													std::cout << "\nPress Any Key to Continue...";
													std::cin.clear();
													std::cin.get();
													std::cin.get();
												}
												break;
											case 2:
												{
													system("clear");
													cout<<"\n ***************** Taylor Method *****************\n";
													int ch =select_Problem();

													switch(ch)
													{
														case 1:
															taylor_method(1);
															break;
														case 2:
															taylor_method(2);
															break;
														case 3:
															taylor_method(3);
															break;
														case 4:
															break;
														default:cout<<"Invalid choice !\n\n";
													}
													std::cout << "\nPress Any Key to Continue...";
													std::cin.clear();
													std::cin.get();
													std::cin.get();
												}
												break;
											case 3:
												{
													system("clear");
													cout<<"\n ***************** For Second Order Methods *****************\n";
													cout<<"1. Improved Tangent Method. \n";
													cout<<"2. Euler-Cauchy Method. \n";
													cout<<"3.Go Back\n";
													cout<<"\nEnter ur choice:";
													int ch;
													std::cin.clear();
													cin>>ch;

													switch(ch)
													{
														case 1:
															{
																system("clear");
																cout<<"\n ***************** Improved TangentMethod Method *****************\n";
																int ch =select_Problem();

																switch(ch)
																{
																	case 1:
																		improved_Tangent_Method(1);
																		break;
																	case 2:
																		improved_Tangent_Method(2);
																		break;
																	case 3:
																		improved_Tangent_Method(3);
																		break;
																	case 4:
																		break;
																	default:cout<<"Invalid choice !\n\n";
																}
																std::cout << "\nPress Any Key to Continue...";
																std::cin.clear();
																std::cin.get();
																std::cin.get();
															}
															break;
														case 2:
															{
																system("clear");
																cout<<"\n ***************** Euler-Cauchy Method *****************\n";
																int ch =select_Problem();

																switch(ch)
																{
																	case 1:
																		euler_cauchy_method(1);
																		break;
																	case 2:
																		euler_cauchy_method(2);
																		break;
																	case 3:
																		euler_cauchy_method(3);
																		break;
																	case 4:
																		break;
																	default:cout<<"Invalid choice !\n\n";
																}
																std::cout << "\nPress Any Key to Continue...";
																std::cin.clear();
																std::cin.get();
																std::cin.get();
															}
															break;
														case 3:
															break;
														default:cout<<"Invalid choice !\n\n";
													}
												}
												break;
											case 4:
												{
													system("clear");
													cout<<"\n ***************** For Third Order Methods *****************\n";
													cout<<"Not yet Devloped !!!!!\n";
												}
												break;
											case 5:
												{
													system("clear");
													cout<<"\n ***************** For Forth Order Methods *****************\n";
													cout<<"Not yet Devloped !!!!!\n";
												}
												break;
											case 6:
												{
													system("clear");
													cout<<"\n ***************** Runge-Kutta Methods *****************\n";
													cout<<"Not yet Devloped !!!!!\n";
												}
												break;
											case 7:
												flag=0;
												break;
											default:cout<<"Invalid choice !\n\n";
										}
										std::cout << "\nPress Any Key to Continue...";
										std::cin.clear();
										std::cin.get();
										std::cin.get();
									}
								}
								break;
							case 2:
								{
									system("clear");
									cout<<"\n ***************** Multistep Step Methods *****************\n";
									cout<<"1. Adams-Bashforth Methods.\n";
									cout<<"2. Nystrom Methods.\n";
									cout<<"3. Adams-Moulton Methods.\n";
									cout<<"4. Milne Methods.\n";
									cout<<"5. Go Back\n";
									cout<<"\nEnter ur choice:";
									int ch;
									std::cin.clear();
									cin>>ch;

									switch(ch)
									{
										case 1:
											{
												system("clear");
												cout<<"\n ***************** Adams-Bashforth Methods *****************\n";
												cout<<"Not yet Devloped !!!!!\n";
											}
											break;
										case 2:
											{
												system("clear");
												cout<<"\n ***************** Nystrom Methods *****************\n";
												cout<<"Not yet Devloped !!!!!\n";
											}
											break;
										case 3:
											{
												system("clear");
												cout<<"\n ***************** Adams-Moulton Methods *****************\n";
												cout<<"Not yet Devloped !!!!!\n";
											}
											break;
										case 4:
											{
												system("clear");
												cout<<"\n ***************** Milne Methods *****************\n";
												cout<<"Not yet Devloped !!!!!\n";
											}
											break;
										case 5:
											break;
										default:cout<<"Invalid choice !\n\n";
									}
									std::cout << "\nPress Any Key to Continue...";
									std::cin.clear();
									std::cin.get();
									std::cin.get();

								}
								break;
							case 3:
								flag=0;
								break;
							default:cout<<"Invalid choice !\n\n";
						}
						std::cout << "\nPress Any Key to Continue...";
						std::cin.clear();
						std::cin.get();
						std::cin.get();
					}
				}
				break;
			case 3:
				{
					system("clear");
					cout<<"\n ***************** Numerical Solutions of PDE *****************\n";
					cout<<"Not yet Devloped !!!!!\n";
					std::cout << "\nPress Any Key to Continue...";
					std::cin.clear();
					std::cin.get();
					std::cin.get();
				}
				break;
			case 4:
				cout<<"\n\nThank You for using LPP System...........\n";
				exit(1);
				break;
			default:cout<<"Invalid choice !\n\n";
		}
		std::cout << "\nPress Any Key to Continue...";
		std::cin.clear();
		std::cin.get();
		std::cin.get();
	}

	return 0;
}