#pragma once
#include<string>
#include<iostream>

struct Zj_Cj
{
	// store a + b*M 
	double a,b;
};
class Simplex
{
private:
	int row,column;
	double *Z,**A,*B;	// A for store constrints matrix ,B for Xb
	int *C,*BVtype; 	// 1 for Xi, 2 for Si,3 for Ai,  	C for store postion of Variable in Z
	double *Ratio;
	Zj_Cj *diff;

public:
	Simplex(void);
	~Simplex();
	void readProblem(std::string fname);
	void printBVType(int pos);
	void displayTable(int);
	void maximizeZ();
};