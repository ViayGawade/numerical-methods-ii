#pragma once

void taylor_method(int eg_no);
void taylor_second_order(int eg_no);
void taylor_third_order(int eg_no);

/*
f(xn,yn)=y'

2nd order:
	Yn+1=Y(xn)+h*f(xn,yn)+(h^2/2)*f'(xn,yn)

3rd order:
	Yn+1=Y(xn)+h*f(xn,yn)+h^2/2*f'(xn,yn)+(h^3/3!)*f''(xn,yn)
*/