#declare Variable
CC=g++
# -wall for show warnings
CFLAGS=-c -Wall	

all:MyOP

MyOP:main.o SimplexBasicOperation.o SimplexMethod.o EulerMethod.o Taylor_Methods.o CommanFuntions.o SecondOrderMethods.o
	$(CC) main.o SimplexBasicOperation.o SimplexMethod.o EulerMethod.o Taylor_Methods.o CommanFuntions.o SecondOrderMethods.o -o MyOP
	
Main.o:main.cpp
	$(CC) $(CFLAGS) main.cpp
	
SimplexBasicOperation.o:SourceCode/SimplexBasicOperation.cpp
	$(CC) $(CFLAGS) SourceCode/SimplexBasicOperation.cpp

SimplexMethod.o:SourceCode/SimplexMethod.cpp
	$(CC) $(CFLAGS) SourceCode/SimplexMethod.cpp

EulerMethod.o:SourceCode/EulerMethod.cpp
	$(CC) $(CFLAGS) SourceCode/EulerMethod.cpp

Taylor_Methods.o:SourceCode/Taylor_Methods.cpp
	$(CC) $(CFLAGS) SourceCode/Taylor_Methods.cpp

CommanFuntions.o:SourceCode/CommanFuntions.cpp
	$(CC) $(CFLAGS) SourceCode/CommanFuntions.cpp

SecondOrderMethods.o:SourceCode/SecondOrderMethods.cpp
	$(CC) $(CFLAGS) SourceCode/SecondOrderMethods.cpp